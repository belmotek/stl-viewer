<img src="./var/svg/logo.svg" alt="Logo" title="Logo" width="128" height="128" /> 

# 3D Models viewer

A simple 3D Models viewer for STL and OBJ file format. To represent the 3d objects we use the OpneGL library.
This program is made with Gambas3 IDE v3.17.3.

## Contributing
The source code is available in the [Gitlab](https://gitlab/belmotek/3D-Model-Viewer) repo.

## Contact

Any bug report or soggestion please contact to: [info@belmotek.net](info@belmotek.net)

## Screenshoots 

<img src="https://i.imgur.com/grfbClo.png" alt="Screenshoot-1" title="Screenshoot-1" /> 

## Gambas project

Gambas is a free development environment and a full powerful development platform based on a Basic interpreter with object extensions.
[http://gambas.sourceforge.net/en/main.html](URL)

## License

![Source code license](./var/svg/license-gpl-v3-blue.svg)

The source code is licensed under GPL v3. Please see the about section for mere details.
