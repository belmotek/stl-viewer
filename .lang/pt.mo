��    <      �  S   �      (     )     1     7     G     M     ^  	   c     m     y     �     �     �     �     �  	   �     �     �     �     �  
                         	   .     8     @  	   ]     g     u     |  
   �     �  
   �     �     �     �     �     �     �     �               '     3     B  	   K     U     f  &   y     �     �     �     �     �     �     �  6   �     -  �   :     
	     	     	     2	     :	     K	     P	     _	  )   n	     �	     �	     �	  !   �	  	   �	     �	  	   �	     �	     
     
     %
     3
     :
     @
     C
     U
     d
      m
     �
     �
     �
     �
     �
     �
     �
     �
               !     /     D     ]     u     �     �     �     �     �     �     �          $     4  *   :     e     s     �     �  F   �     �     )             6          	   9   +   2                      -                 #   7      (       *       
       :   5   $   3                           !                      ;   1                                    4   %                   &             '   0   ,   .   <   /      "      8           3D view About Add new texture Angle Angle in degrees Axis Back view Bottom view Can't determine array type Cancel Center point Close Config change succefully Credits End angle Excluded Export as image Filter First point Front view Height Help Hi Isometric view Left view License More than one capital letter Open file Picture files Radius Repeated Right view STL FIles viewer STL Viewer Save Save sketch as Scale Second point Select File Select background color Select directory Select part color Select point Select text Select texture Settings Show axis Space at the end Space at the start Spaces at the beginning and at the end Start angle Text Toggle Camera Orthographic Top view Unknown Use texture Vector files Which tool should be used to import this type of file? Width factor Project-Id-Version: STL Viewer 0.1.0
PO-Revision-Date: 2022-12-09 10:20 UTC
Last-Translator: Tradukisto
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Vista 3D Sobre Acrescentar nova textura Ângulo Ângulo em graus Eixo Vista traseira Vista do fundo Não consegue determinar o tipo de matriz Cancelar Ponto central Fechar Configurar a mudança com sucesso Créditos Ângulo final Excluído Exportar como imagem Filtro Primeiro ponto Vista frontal Altura Ajuda Hi Vista isométrica Vista esquerda Licença Mais do que uma letra maiúscula Abrir ficheiro Ficheiros fotográficos Radius Repito Vista direita Visualizador de STL FIles Visualizador STL Guardar Salvar esboço como Balança Segundo ponto Seleccionar ficheiro Seleccionar cor de fundo Seleccionar directório Seleccione a cor da peça Selecione o arquivo Seleccionar texto Seleccionar textura Definições Mostrar eixo Espaço no final Espaço no início Espaços no início e no fim Ângulo inicial Texto Alternar Câmara Ortográfica Ortográfica Vista de cima Desconhecido Usar textura Ficheiros vectoriais Que ferramenta deve ser utilizada para importar este tipo de ficheiro? Factor de largura 