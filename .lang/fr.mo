��    <      �  S   �      (     )     1     7     G     M     ^  	   c     m     y     �     �     �     �     �  	   �     �     �     �     �  
                         	   .     8     @  	   ]     g     u     |  
   �     �  
   �     �     �     �     �     �     �     �               '     3     B  	   K     U     f  &   y     �     �     �     �     �     �     �  6   �     -  �   :     
	     	     	     ;	     A	     R	     V	  
   c	  ,   n	     �	     �	     �	  #   �	     �	     �	     �	     �	     
     
     '
     3
     ;
     @
     H
     Y
     g
     o
     �
     �
     �
     �
     �
     �
     �
     �
               '     4  +   M     y  %   �     �     �     �                     ,     =     \     m  "   s     �     �     �     �  >   �          )             6          	   9   +   2                      -                 #   7      (       *       
       :   5   $   3                           !                      ;   1                                    4   %                   &             '   0   ,   .   <   /      "      8           3D view About Add new texture Angle Angle in degrees Axis Back view Bottom view Can't determine array type Cancel Center point Close Config change succefully Credits End angle Excluded Export as image Filter First point Front view Height Help Hi Isometric view Left view License More than one capital letter Open file Picture files Radius Repeated Right view STL FIles viewer STL Viewer Save Save sketch as Scale Second point Select File Select background color Select directory Select part color Select point Select text Select texture Settings Show axis Space at the end Space at the start Spaces at the beginning and at the end Start angle Text Toggle Camera Orthographic Top view Unknown Use texture Vector files Which tool should be used to import this type of file? Width factor Project-Id-Version: STL Viewer 0.1.0
PO-Revision-Date: 2022-12-09 10:20 UTC
Last-Translator: Tradukisto
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Vue 3D À propos de Ajouter une nouvelle texture Angle Angle en degrés Axe Vue arrière Vue du bas Impossible de déterminer le type de tableau Annuler Point central Fermer Changement de configuration réussi Crédits Angle de fin Exclu Exporter en tant qu'image Filtre Premier point Vue de face Hauteur Aide Bonjour Vue isométrique Vue de gauche Licence Plus d'une lettre majuscule Ouvrir un fichier Fichiers image Rayon Répétées Vue de droite Visualiseur de fichiers STL Visualisateur STL Enregistrer Enregistrer dessin sous Échelle Second point Sélectionner le fichier Sélectionner la couleur de l'arrière-plan Sélectionner le répertoire Sélectionner la couleur de la pièce Point de sélection Sélectionner le texte Sélectionner la texture Paramètres Afficher l'axe Espace à la fin Espace au début Espaces au début et à la fin Angle de départ Texte Basculer la caméra orthographique Vue de dessus Inconnue Utiliser la texture Fichiers vectoriels Quel outil doit-on utiliser pour importer ce type de fichier ? Facteur de largeur 