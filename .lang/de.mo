��    <      �  S   �      (     )     1     7     G     M     ^  	   c     m     y     �     �     �     �     �  	   �     �     �     �     �  
                         	   .     8     @  	   ]     g     u     |  
   �     �  
   �     �     �     �     �     �     �     �               '     3     B  	   K     U     f  &   y     �     �     �     �     �     �     �  6   �     -  �   :  
   
	     	     	     3	     :	     I	     O	     \	  $   n	  	   �	     �	  
   �	  (   �	  	   �	  	   �	     �	      
     
     
     "
     0
     8
     >
     D
     Y
     g
     n
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     	                1     M     d     |     �     �     �     �     �     �  !   �          #     (     H  	   Y     c     t  P   �     �     )             6          	   9   +   2                      -                 #   7      (       *       
       :   5   $   3                           !                      ;   1                                    4   %                   &             '   0   ,   .   <   /      "      8           3D view About Add new texture Angle Angle in degrees Axis Back view Bottom view Can't determine array type Cancel Center point Close Config change succefully Credits End angle Excluded Export as image Filter First point Front view Height Help Hi Isometric view Left view License More than one capital letter Open file Picture files Radius Repeated Right view STL FIles viewer STL Viewer Save Save sketch as Scale Second point Select File Select background color Select directory Select part color Select point Select text Select texture Settings Show axis Space at the end Space at the start Spaces at the beginning and at the end Start angle Text Toggle Camera Orthographic Top view Unknown Use texture Vector files Which tool should be used to import this type of file? Width factor Project-Id-Version: STL Viewer 0.1.0
PO-Revision-Date: 2022-12-09 10:20 UTC
Last-Translator: Tradukisto
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 3D-Ansicht Über Neue Textur hinzufügen Winkel Winkel in Grad Achse Rückansicht Ansicht von unten Array-Typ kann nicht bestimmt werden Abbrechen Mittelpunkt Schließen Erfolgreiche Änderung der Konfiguration Impressum Endwinkel Ausgeschlossen Als Bild exportieren Filter Punkt Vorderansicht Gewicht Hilfe Hallo Isometrische Ansicht Linke Ansicht Lizenz Mehr als ein Großbuchstabe Datei öffnen Bilddateien Radius Wiederholte Ansicht rechts STL FIles Betrachter STL-Betrachter Speichern Skizze speichern als Maßstab Zweiter Punkt Datei auswählen Hintergrundfarbe auswählen Verzeichnis auswählen Bauteilfarbe auswählen Datei aussuchen Text auswählen Textur auswählen Einstellungen Achse anzeigen Leerzeichen am Ende Leerzeichen am Anfang Leerzeichen am Anfang und am Ende Startwinkel Text Kamera orthografisch umschalten Ansicht von oben Unbekannt Textur verwenden Vektor-Dateien Welches Werkzeug sollte verwendet werden, um diese Art von Datei zu importieren? Breitenfaktor 