��    <      �  S   �      (     )     1     7     G     M     ^  	   c     m     y     �     �     �     �     �  	   �     �     �     �     �  
                         	   .     8     @  	   ]     g     u     |  
   �     �  
   �     �     �     �     �     �     �     �               '     3     B  	   K     U     f  &   y     �     �     �     �     �     �     �  6   �     -  �   :     
	     	     #	     @	     G	     W	     \	     m	  *   }	     �	     �	     �	  &   �	     �	     �	     
     
     &
     -
     9
     H
     P
     V
     [
     l
     }
     �
     �
     �
     �
     �
     �
     �
     �
               '     -     ;      M     n     �     �     �     �     �     �     �     
          9     I     O     n     ~     �     �  @   �     �     )             6          	   9   +   2                      -                 #   7      (       *       
       :   5   $   3                           !                      ;   1                                    4   %                   &             '   0   ,   .   <   /      "      8           3D view About Add new texture Angle Angle in degrees Axis Back view Bottom view Can't determine array type Cancel Center point Close Config change succefully Credits End angle Excluded Export as image Filter First point Front view Height Help Hi Isometric view Left view License More than one capital letter Open file Picture files Radius Repeated Right view STL FIles viewer STL Viewer Save Save sketch as Scale Second point Select File Select background color Select directory Select part color Select point Select text Select texture Settings Show axis Space at the end Space at the start Spaces at the beginning and at the end Start angle Text Toggle Camera Orthographic Top view Unknown Use texture Vector files Which tool should be used to import this type of file? Width factor Project-Id-Version: STL Viewer 0.1.0
PO-Revision-Date: 2022-12-09 10:20 UTC
Last-Translator: Tradukisto
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Vista 3D Informazioni su Aggiungere una nuova texture Angolo Angolo in gradi Asse Vista posteriore Vista dal basso Impossibile determinare il tipo di matrice Annulla Punto centrale Chiudi Modifica della configurazione riuscita Crediti Angolo finale Escluso Esportazione come immagine Filtro Primo punto Vista frontale Altezza Aiuto Ciao Vista isometrica Vista a sinistra Licenza Più di una lettera maiuscola Aprire un file File immagine Raggio Ripetuto Vista destra Visualizzatore di file STL Visualizzatore STL Salva Salva lo schizzo come Scala Secondo punto Seleziona il file Seleziona il colore dello sfondo Seleziona directory Seleziona il colore della parte Seleziona il file Selezionare il testo Selezionare la texture Impostazioni Mostra asse Spazio alla fine Spazio all'inizio Spazi all'inizio e alla fine Angolo iniziale Testo Alterna telecamera ortografica Vista dall'alto Sconosciuto Usa la texture File vettoriali Quale strumento si deve usare per importare questo tipo di file? Fattore di larghezza 