��    <      �  S   �      (     )     1     7     G     M     ^  	   c     m     y     �     �     �     �     �  	   �     �     �     �     �  
                         	   .     8     @  	   ]     g     u     |  
   �     �  
   �     �     �     �     �     �     �     �               '     3     B  	   K     U     f  &   y     �     �     �     �     �     �     �  6   �     -  �   :     
	  	   	     	     3	     ;	     M	     Q	     a	  (   p	     �	     �	     �	  #   �	  	   �	     �	     �	     �	     

     
     
     -
     2
     8
     =
     O
     _
     h
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
                    /     J     c     ~     �     �     �     �     �     �      �          #     )     G     V     b     o  F   �     �     )             6          	   9   +   2                      -                 #   7      (       *       
       :   5   $   3                           !                      ;   1                                    4   %                   &             '   0   ,   .   <   /      "      8           3D view About Add new texture Angle Angle in degrees Axis Back view Bottom view Can't determine array type Cancel Center point Close Config change succefully Credits End angle Excluded Export as image Filter First point Front view Height Help Hi Isometric view Left view License More than one capital letter Open file Picture files Radius Repeated Right view STL FIles viewer STL Viewer Save Save sketch as Scale Second point Select File Select background color Select directory Select part color Select point Select text Select texture Settings Show axis Space at the end Space at the start Spaces at the beginning and at the end Start angle Text Toggle Camera Orthographic Top view Unknown Use texture Vector files Which tool should be used to import this type of file? Width factor Project-Id-Version: STL Viewer 0.1.0
PO-Revision-Date: 2022-12-09 10:20 UTC
Last-Translator: Tradukisto
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Vista 3D Acerca de Añadir nueva textura Ángulo Ángulo en grados Eje Vista posterior Vista inferior No se puede determinar el tipo de matriz Cancelar Centro Cerrar Cambio de configuración con éxito Créditos Ángulo final Excluido Exportar como imagen Filtrar Primer punto Vista frontal Alto Ayuda Hola Vista isométrica Vista izquierda Licencia Más de una letra mayúscula Abrir archivo Archivos de imagen Rádio Repetición de Vista derecha Visor de archivos STL Visor STL Guardar Guardar dibujo como Escala Segundo punto Seleccionar archivo Seleccionar color de fondo Seleccione el directorio Seleccionar color de pieza Seleccione un punto Seleccionar texto Seleccionar textura Ajustes Mostrar eje Espacio al final Espacio al principio Espacios al principio y al final Ángulo inicial Texto Alternar cámara ortográfica Vista superior Desconocido Usar textura Archivos vectoriales ¿Qué herramienta debe utilizarse para importar este tipo de archivo? Factor de anchura 