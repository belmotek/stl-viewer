#!/bin/bash
# SVG files cleaner 
# by Belmotek 2022
# Lic: GPL v3
# scour

ls --recursive *.[Ss][Vv][Gg] | while read -r FILE
do
    scour "$FILE" "../$FILE"
done

